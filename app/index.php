
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="Hideez Wallet">
<!--         <link rel="shortcut icon" href="assets/images/favicon.ico"> -->
        <title>Hideez Wallet</title>

        <!-- Google Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Quattrocento+Sans:400,700|Roboto:400,500,700" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800&amp;subset=cyrillic" rel="stylesheet">

        <!-- Bootstrap core CSS -->
        <link href="css/bootstrap.min.css" rel="stylesheet">

        <!-- Materialdesign css -->
        <link href="css/materialdesignicons.min.css" rel="stylesheet">
        <link href="css/mobiriseicons.css" rel="stylesheet">

        <!-- Custom styles for this template -->
        <link href="css/menu.css" rel="stylesheet">
        <link href="css/style.css" rel="stylesheet">
        <link href="fonts/stylesheet.css" rel="stylesheet">

        <link href="css/main.css" rel="stylesheet">

        <!-- Animation -->
        <link href="css/animate.css" rel="stylesheet">

    </head>
    <body>
        <div class="preloader">
            <div class="sk-folding-cube">
                <div class="sk-cube1 sk-cube"></div>
                <div class="sk-cube2 sk-cube"></div>
                <div class="sk-cube4 sk-cube"></div>
                <div class="sk-cube3 sk-cube"></div>
            </div>
        </div>
        <!-- Navigation Bar-->
        <header id="topnav" class="defaultscroll fixed-top sticky">
            <div class="container">
                <!-- Logo container-->
                <div>
                    <a href="index.html" class="logo text-uppercase">
                        <img src="images/logo.png" alt="Logo"/>
                    </a>
                </div>
                <!-- End Logo container-->
                <div class="menu-extras">
                    <div class="menu-item">
                        <!-- Mobile menu toggle-->
                        <a class="navbar-toggle">
                            <div class="lines">
                                <span></span>
                                <span></span>
                                <span></span>
                            </div>
                        </a>
                        <!-- End mobile menu toggle-->
                    </div>
                </div>
                <div id="navigation">
                    <!-- Navigation Menu-->
                    <ul class="navigation-menu">
                        <li class="active">
                            <a href="#home">Usability</a>
                        </li>
                        <li class="">
                            <a href="#features">Secure Features</a>
                        </li>
                        <li class="">
                            <a href="#hideez-usp">Hideez USP</a>
                        </li>
                        <li class="">
                            <a href="#how-hw-works">How it works</a>
                        </li>
                        <li class="">
                            <a href="#specification">Specifications</a>
                        </li>
                        <li class="">
                            <a href="#footer">Contacts</a>
                        </li>
                    </ul>
                </div>
            </div>
        </header>  

        <section class="bg-home home-height-half" id="home">
            <div class="bg-overlay wow slideInLeft animated"></div>
            <div class="bg-overlay-part"></div>
            <div class="horizon-elem">
                <ul>
                    <li>Secure storage</li>
                    <li>Privacy</li>
                    <li>Bluetooth encryption</li>
                </ul>
            </div>
            <div class="home-center">
                <div class="home-desc-center">
                    <div class="container">
                        <div class="row justify-content-end">
                            <div class="col-9 col-sm-8 offset-sm-2 col-md-7 offset-md-2 col-lg-5 offset-lg-2">
                                <div class="main-title wow fadeInDown animated">
                                    <h1 class="home-title">Hideez Wallet</h1>
                                    <p class="pt-4 home-sub-title mx-auto">Your Wireless Hardware Crypto-Assets Keeper</p>
                                    <div class="buttons-store">
                                        <a href="#" class="btn">
                                            Get informed
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="systems">
                    <ul class="characteristic">
                       <li class="pt-1">
                           <img src="images/img-17.png">
                           <p>Windows 7+</p>
                       </li>
                       <li class="pt-1">
                           <img src="images/img-18.png">
                           <p>Android 4.4+</p>
                       </li>
                       <li class="pt-1">
                           <img src="images/img-19.png">
                           <p>iOS 9.3+</p>
                       </li>
                       <li class="pt-1">
                           <img src="images/img-20.png">
                           <p>MacOS 10.11+</p>
                       </li>
                   </ul>
                </div>
            </div>
        </section>

        <section class="section" id="simplicity-and-usability">
            <div class="simpl-and-usabl" id="features">
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="col-lg-6">
                            <div class="title text-center">
                                <h2>Simplicity & Usability</h2>
                            </div>
                        </div>
                    </div>
                    <div class="row mt-4 pt-4 justify-content-center">
                        <div class="col-10 col-md-12 col-lg-4 mt-2">
                            <div class="simpl-and-usabl-desc custom-width num-one">
                                <h5>Secure storage</h5>
                                <p class="mt-3">Hideez Key is based on a Nordic SOC with individual block protection. It is controlled with special registers which can only be wiped on hard reset. The firmware integrity is guaranteed by cryptographic attestation.</p>
                            </div>
                        </div>
                        <div class="col-10 col-md-12 col-lg-4 mt-2">
                            <div class="simpl-and-usabl-desc custom-width num-two">
                                <h5>Privacy</h5>
                                <p class="mt-3">Your private keys are never exposed neither to Hideez cloud, nor to a third party: they are always securely stored in the NORDIC SOC. Your wallet remains decentralized with Hideez, you are your own bank.</p>
                            </div>
                        </div>
                        <div class="col-10 col-md-12 col-lg-4 mt-2">
                            <div class="simpl-and-usabl-desc custom-width num-three">
                                <h5>Bluetooth encryption</h5>
                                <p class="mt-3">Bluetooth encryption is enabled via bonding with secure private key. Operates with symmetric AES-128 key encryption.Upper layer session encryption with symmetric AES-128 key between Hideez Wallet and application on device.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="round-elem" id="hideez-usp">
                <div class="container custom-container">
                        <div class="row mt-4 pt-4 justify-content-center">
                            <div class="col-md-5 col-lg-4 mt-2">
                                <div class="simpl-and-usabl-desc wow fadeIn animated">
                                     <img src="images/img-1.png" alt="" class="img-fluid  d-block wow flipInX animated">
                                    <h5>Bitcoin and altcoins</h5>
                                    <p class="mt-3">Hold all assets in the same hardware wallet: Bitcoin, Bcash, Zcash, Litecoin and others…</p>
                                </div>
                            </div>
                            <div class="col-md-5 col-lg-4 mt-2">
                                <div class="simpl-and-usabl-desc wow fadeIn animated">
                                    <img src="images/img-2.png" alt="" class="img-fluid  d-block wow flipInX animated">
                                    <h5>Ethereum</h5>
                                    <p class="mt-3">Full support for Ethereum and ERC-20 tokens.</p>
                                </div>
                            </div>
                            <div class="col-md-5 col-lg-4 mt-2">
                                <div class="simpl-and-usabl-desc wow fadeIn animated">
                                    <img src="images/img-3.png" alt="" class="img-fluid  d-block wow flipInX animated">
                                    <h5>Wireless</h5>
                                    <p class="mt-3">Encrypted Bluetooth connection and custom-built full stack authentication protects the data traffic without cables.</p>
                                </div>
                            </div>
                            <div class="col-md-5 col-lg-4 mt-2">
                                <div class="simpl-and-usabl-desc wow fadeIn animated">
                                     <img src="images/img-4.png" alt="" class="img-fluid  d-block wow flipInX animated">
                                    <h5>Password Manager</h5>
                                    <p class="mt-3">Store your credentials securely and keep them physically separated from your main device.…</p>
                                </div>
                            </div>
                            <div class="col-md-5 col-lg-4 mt-2">
                                <div class="simpl-and-usabl-desc wow fadeIn animated">
                                    <img src="images/img-5.png" alt="" class="img-fluid  d-block wow flipInX animated">
                                    <h5>Password–less login</h5>
                                    <p class="mt-3">Automated password input for sites and applications.</p>
                                </div>
                            </div>
                            <div class="col-md-5 col-lg-4 mt-2">
                                <div class="simpl-and-usabl-desc wow fadeIn animated">
                                    <img src="images/img-6.png" alt="" class="img-fluid  d-block wow flipInX animated">
                                    <h5>2–Factor Authentication</h5>
                                    <p class="mt-3">Generate one-time passwords for two-factor authentication, crypto exegeses and banking.</p>
                                </div>
                            </div>
                            <div class="col-md-5 col-lg-4 mt-2">
                                <div class="simpl-and-usabl-desc wow fadeIn animated">
                                     <img src="images/img-7.png" alt="" class="img-fluid  d-block wow flipInX animated">
                                    <h5>Invisible PIN</h5>
                                    <p class="mt-3">Multi-functional button allows confirm any instance of authentication, thus preventing undesired authentications.</p>
                                </div>
                            </div>
                            <div class="col-md-5 col-lg-4 mt-2">
                                <div class="simpl-and-usabl-desc wow fadeIn animated">
                                    <img src="images/img-8.png" alt="" class="img-fluid  d-block wow flipInX animated">
                                    <h5>TouchID</h5>
                                    <p class="mt-3">Use fingerprint identification to activate the Hideez and secure login.</p>
                                </div>
                            </div>
                            <div class="col-md-5 col-lg-4 mt-2">
                                <div class="simpl-and-usabl-desc wow fadeIn animated">
                                    <img src="images/img-9.png" alt="" class="img-fluid  d-block wow flipInX animated">
                                    <h5>Backup </h5>
                                    <p class="mt-3">Use another Hideez device to backup your Hideez Wallet or create a backup copy in an encrypted file and keep it in a secure place.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
        </section>

        <section class="section" id="how-hw-works">
            <div class="how-hw-works">
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="col-lg-6">
                            <div class="title text-center ">
                                <h2 class="wow fadeIn  animated">How Hideez Wallet Works</h2>
                            </div>
                        </div>
                    </div>
                    <div class="row mt-4 pt-4 vertical-content">
                        <div class="col-sm-9 col-md-8 col-lg-4 mt-2 wow fadeInLeft animated">
                            <div class="works-desc left-part vertical-content">
                                 <p>Bridging Application is installed on your PC and Smartphone</p><span>.01</span>
                            </div>
                            <div class="works-desc left-part vertical-content">
                                 <p>Connection of Hideez Wallet is done via Bluetooth with your PC and Smartphone</p><span>.02</span>
                            </div>
                            <div class="works-desc left-part vertical-content">
                                 <p>Connect to the Software Wallet — Copay, Electrum, other</p><span>.03</span>
                            </div>
                        </div>
                        <div class="col-lg-4 mt-2 text-center wow zoomInUp animated">
                            <div>
                                 <img src="images/img-10.png" alt="" class="img-fluid mx-auto d-block wow flipInX animated">
                            </div>
                        </div>
                        <div class="col-sm-9 col-md-8 col-lg-4 mt-2 wow fadeInRight animated">
                            <div class="works-desc right-part vertical-content">
                                <span>.04</span>
                                 <p class="justify-content-end">Bridging Application is installed on your PC and Smartphone</p>
                            </div>
                            <div class="works-desc right-part vertical-content">
                                <span>.05</span>
                                 <p>Connection of Hideez Wallet is done via Bluetooth with your PC and Smartphone</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="section" id="specification">
            <div class="wrapper-specification">
                <div class="container">
                    <div class="row mt-4 pt-4 vertical-content">
                        <div class="col-md-4 col-lg-3 mt-2 swift-block">
                            <div class="container-left wow slideInLeft animated">
                                <p>DIMENSION & WEIGHT</p>
                               <img src="images/img-14.png">
                               <ul class="characteristic mt-2">
                                   <li class="vertical-content pt-4">
                                       <img src="images/img-15.png">
                                       <p>32,5х 32,5 х 9,5 mm</p>
                                   </li>
                                   <li class="vertical-content pt-2">
                                       <img src="images/img-16.png">
                                       <p>9 g</p>
                                   </li>
                               </ul>
                               <p class="mt-2">DESKTOP & MOBILE COMPATIBILITY</p>
                               <ul class="characteristic">
                                   <li class="vertical-content pt-2">
                                       <img src="images/img-17.png">
                                       <p>Windows 7+</p>
                                   </li>
                                   <li class="vertical-content pt-2">
                                       <img src="images/img-18.png">
                                       <p>Android 4.4+</p>
                                   </li>
                                   <li class="vertical-content pt-2">
                                       <img src="images/img-19.png">
                                       <p>iOS 9.3+</p>
                                   </li>
                                   <li class="vertical-content pt-2">
                                       <img src="images/img-20.png">
                                       <p>MacOS 10.11+</p>
                                   </li>
                               </ul>
                            </div>
                        </div>
                        <div class="col-md-8 col-lg-8 mt-2 no-swift-block">
                            <div class="container-right">
                                <div class="vertical-content">
                                    <img src="images/img-11.png" alt="MICROCONTROLLERS" class="wow flipInX  animated">
                                    <p><span>MICROCONTROLLERS</span><br>Nordic nRF51822-QFAC Chip</p>
                                </div>
                                <div class="vertical-content">
                                    <img src="images/img-12.png" alt="BATTERY" class="wow flipInX  animated">
                                    <p><span>BATTERY</span><br>CR2032 (up to 6 month)</p>
                                </div>
                                <div class="vertical-content">
                                    <img src="images/img-13.png" alt="CONNECTOR" class="wow flipInX  animated">
                                    <p><span>CONNECTOR</span><br>Bluetooth 4.1+</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-8 offset-lg-4 mt-2 swift-block-bottom">
                            <div class="multi-currency wow fadeIn animated">
                                <p>MULTI–CURRENCY</p>
                                <div class="vertical-content">
                                    <img src="images/img-23.png" alt="MICROCONTROLLERS">
                                    <img src="images/img-24.png" alt="MICROCONTROLLERS">
                                    <img src="images/img-25.png" alt="MICROCONTROLLERS">
                                    <img src="images/img-26.png" alt="MICROCONTROLLERS">
                                    <img src="images/img-27.png" alt="MICROCONTROLLERS">
                                    <img src="images/img-28.png" alt="MICROCONTROLLERS">
                                    <img src="images/img-29.png" alt="MICROCONTROLLERS">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="section bg-light" id="contact-us"> 
            <div class="wrapper-start-using">          
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="col-lg-8">
                            <div class="testimonial-box text-center">
                                <h2>Contact and Support</h2>
                                <p class="mt-4">Join us and collaborate on projects related to cryptocurrency and security via <span><a href="mailto:info@hideez.com">info@hideez.com</a></span></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div> 
        </section>
        <footer>
            <section id="footer"> 
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="col-sm-4 col-md-4 col-lg-3 mt-2 text-center">
                            <!-- Logo container-->
                            <div>
                                <a href="index.html" class="logo">
                                    <img src="images/logo-footer.png" alt="Logo"/>
                                </a>
                            </div>
                        <!-- End Logo container-->
                        </div>
                        <div class="col-sm-12 col-lg-4 offset-lg-5 text-center mt-2 ">
                            <p class="footer-title">© 2018 Hideez Wallet. All right reserved.</p>
                        </div>
                    </div>
                </div>
            </section>
        </footer>

        <script src="js/jquery.min.js"></script>
<!--         <script src="js/popper.min.js"></script> -->

        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
        <script src="js/jquery.easing.min.js"></script>
        <script src="js/wow.min.js" type="text/javascript"></script>
        <script src="js/scrollspy.min.js" type="text/javascript"></script>
        <script src="js/jquery.app.js"></script>
        <script>
            new WOW().init({
                mobile: true,
            });
        </script>
    </body>
</html>